#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct pecado{
	char nombre[45];
	int valor;
	struct pecado *prox; 
}pe;
typedef struct persona_pecado{
	char nombre[45],fecha[45];
	int valor;
	struct persona_pecado *prox; 
}pp;


typedef struct categoria_pecados{
	char nombre[45];
	int valor;
	struct categoria_pecados *proxc;
	pe *proxp; 
}cape;

typedef struct bondad{
	char nombre[45];
	int valor;
	struct bondad *prox; 
}bo;
typedef struct persona_bondad{
	char nombre[45], fecha[45];
	int valor;
	struct persona_bondad *prox; 
}pb;

typedef struct categoria_bondad{
	char nombre[45];
	int valor;
	struct categoria_bondad *proxc;
	bo *proxb; 
}cabo;

typedef struct personas{
	char nombre[45], nacimiento[45], genero, pais[45];
	int inmaldad,inbondad,consecutivo,inexpiacion;
	pp *maldad;
	pb* bondad;
	struct personas* proxper;
}per;
typedef struct vivos{
	char fecha[45];
	struct vivos *proxv;
	per * proxpe;
	
}vi;
void nacimiento (vi ** p,char nom[],char fecha[],char g, char pa[],int *con){
	per * nuevop,*auxp;
	vi * aux=*p,*nuevov;
	int encontro=0;
	while((aux) && (!encontro)){
		if (!strcoll(aux->fecha,fecha)){
			nuevop=(per*)malloc(sizeof(per));
			strcpy(nuevop->nombre,nom);
			strcpy(nuevop->nacimiento,fecha);
			nuevop->inbondad=0;
			nuevop->inmaldad=0;
			nuevop->inexpiacion=0;
			nuevop->maldad=NULL;
			nuevop->bondad=NULL;
			nuevop->genero=g;
			strcpy(nuevop->pais,pa);
			auxp=aux->proxpe;
			while(auxp->proxper!=NULL){
				auxp=auxp->proxper;
			}
			nuevop->consecutivo=auxp->consecutivo+1;
			*con=nuevop->consecutivo;
			auxp->proxper=nuevop;
			nuevop->proxper=NULL;
			encontro=1;
		}
		aux=aux->proxv;
	}
	if (!encontro){
		nuevov =(vi*)malloc(sizeof(vi));
		strcpy(nuevov->fecha,fecha);
		nuevov->proxv=*p;
		*p=nuevov;
		nuevop=(per*)malloc(sizeof(per));
		strcpy(nuevop->nombre,nom);
		strcpy(nuevop->nacimiento,fecha);
		nuevop->consecutivo=1;
		*con=1;
		nuevop->genero=g;
		strcpy(nuevop->pais,pa);
		nuevov->proxpe=nuevop;
		nuevop->inbondad=0;
		nuevop->inmaldad=0;
		nuevop->inexpiacion=0;
		nuevop->maldad=NULL;
		nuevop->bondad=NULL;
		nuevop->proxper=NULL;
	}

}
void mostrar_persona(vi*p){
	vi *aux=p;
	per *auxp;
	while(aux){
		auxp=aux->proxpe;
		while(auxp){
			printf("nacimiento: %s nombre: %s genero: %c pais: %s maldad: %d bondad: %d ",auxp->nacimiento,auxp->nombre,auxp->genero,auxp->pais,auxp->inmaldad,auxp->inbondad);
			printf("arrpentimiento: %d consecutivo: %d \n",auxp->inexpiacion,auxp->consecutivo);
			auxp=auxp->proxper;
		}
		aux=aux->proxv; 
	}
}
per* buscar_persona(vi * p,char fecha[],int con){
	vi *aux=p;
	per * auxp;
	while(aux){
		if(!strcoll(aux->fecha,fecha)){
			auxp=aux->proxpe;
			while(auxp){ 
				if(auxp->consecutivo ==con)
					return auxp;
				auxp=auxp->proxper; 
				} 
		}
		aux=aux->proxv; 
	}
	return NULL;
}
pe*buscar_pecado(cape * p,char pecado[]){
	cape*aux=p;
	pe * auxp;
	while(aux){
		auxp=aux->proxp;
		while(auxp){
			if(!strcoll(auxp->nombre,pecado))
				return auxp;
			auxp=auxp->prox;
		}
		aux=aux->proxc;
	} 
	return NULL; 
	}
int buscar_valor_categoria_pecado(cape *p,pe*q){
	cape *aux=p;
	pe* auxp;
	while(aux){
		auxp=aux->proxp;
		while(auxp){
			if(auxp == q)
				return aux->valor;
			auxp=auxp->prox;
		}
	aux=aux->proxc;
	}
} 
void cometer_falta(vi *p,cape * q,char pecado[],char fecha_pecado[],char fecha[],int consecutivo,int * falla){
	pe *auxp=buscar_pecado(q,pecado);pp*nuevo;
	if(auxp){ 
		per *aux=buscar_persona(p,fecha,consecutivo); 
		if(aux){
			nuevo=(pp*)malloc(sizeof(pp));
			strcpy(nuevo->nombre,pecado);
			nuevo->valor=auxp->valor+buscar_valor_categoria_pecado(q,auxp);
			nuevo->prox=aux->maldad;
			strcpy(nuevo->fecha,fecha_pecado);
			aux->maldad=nuevo;
			aux->inmaldad+=nuevo->valor;
			*falla=0;
		}else {printf("la persona no a nacido");*falla=1;}
	}else {printf("cree el pecado primero");*falla=1; }
}
bo*buscar_bondad(cabo * p,char bondad[]){
	cabo*aux=p;
	bo * auxp;
	while(aux){
		auxp=aux->proxb;
		while(auxp){
			if(!strcoll(auxp->nombre,bondad))
				return auxp;
			auxp=auxp->prox;
		}
		aux=aux->proxc;
	} 
	return NULL; 
	}
int buscar_valor_categoria_bondad(cabo *p,bo*q){
	cabo *aux=p;
	bo* auxp;
	while(aux){
		auxp=aux->proxb;
		while(auxp){
			if(auxp == q)
				return aux->valor;
			auxp=auxp->prox;
		}
	aux=aux->proxc;
	}
} 
void realizar_buena_accion(vi *p,cabo * q,char bondad[],char fecha_bondad[],char fecha[],int consecutivo,int * falla){
	bo *auxp=buscar_bondad(q,bondad); pb *nuevo;
	if(auxp){ 
		per *aux=buscar_persona(p,fecha,consecutivo); 
		if(aux){
			nuevo=(pb*)malloc(sizeof(pb));
			strcpy(nuevo->nombre,bondad);
			nuevo->valor=auxp->valor+buscar_valor_categoria_bondad(q,auxp);
			nuevo->prox=aux->bondad;
			strcpy(nuevo->fecha,fecha_bondad);
			aux->bondad=nuevo;
			aux->inbondad+=nuevo->valor;
			*falla=0;
		}else{ printf("la persona no a nacido");*falla=1;}
	}else {printf("cree la buena accion primero");*falla=1;} 
}
void insertar_categoria_pecado (char ca[], int x, cape **p)
{
	cape *nuevo;
	nuevo = (cape*)malloc(sizeof(cape));
	nuevo->valor = x;
	strcpy(nuevo->nombre,ca);
	nuevo->proxc = *p;
	nuevo->proxp = NULL ;
	*p = nuevo;
}

void insertar_categoria_bondad (char ca[], int x, cabo **p)
{
	cabo *nuevo;
	nuevo = (cabo*)malloc(sizeof(cabo));
	nuevo->valor = x;
	strcpy(nuevo->nombre,ca);
	nuevo->proxc =*p;
	nuevo->proxb = NULL;
	*p = nuevo;
}

void insertar_pecado (char x[], int y, char cate[], cape *q,int * bool){
	cape *aux=q,*h=NULL;
	*bool=0;
	while (aux)
	{ 
		if (!strcoll(aux->nombre,cate))
			h=aux;
		aux=aux->proxc; 	
	} 
		if (h!=NULL)
		{
			pe *nuevo;
			nuevo = (pe*)malloc(sizeof(pe));
			nuevo->valor = y;
			strcpy(nuevo->nombre,x);
			nuevo->prox = h->proxp;
			h->proxp = nuevo;
			*bool=1;
		}
		else
			printf("No se encontro categoria \n");	

}

void insertar_bondad (char x[], int y, char cate[], cabo *q,int * bool){ 
	cabo *aux=q,*h=NULL;
	*bool=0;
	while (aux)	{ 
	if (!strcoll(aux->nombre,cate))
		h=aux;
	aux=aux->proxc;
	} 
	if (h)
	{
		bo *nuevo;
		nuevo = (bo*)malloc(sizeof(bo));
		nuevo->valor = y;
		strcpy(nuevo->nombre,x);
		nuevo->prox = h->proxb;
		h->proxb = nuevo;
		*bool=1;
	}
	else
		printf("No se encontro categoria \n");

}
void mostrar_categoria_pecado (cape*p){
	cape *aux=p;
	printf("categorias de pecados.\n");
	printf("categorias: ");
	while(aux){
		printf("%s valor :%d\n",aux->nombre, aux->valor);
		aux=aux->proxc;
	}
}
void mostrar_pecados_categoria (cape*p,char nom[]){
	cape *aux=p;
	printf("pecados por categoria\n");
	printf("categoria %s valor:%d\n",aux->nombre,aux->valor);
	while(aux){
		if(!strcoll(aux->nombre,nom)){
			pe*auxp=aux->proxp;
			printf("pecados: ");
			while(auxp){
				printf("%s valor:%d, ",auxp->nombre,auxp->valor );
				auxp=auxp->prox;
			}
		}
		aux=aux->proxc;
	}
	printf("\n");
}
void mostrar_pecados_completa (cape*p){
	cape *aux=p;
	FILE *ar;
	char nombre[]="data.txt";
	ar=fopen(nombre,"w");
	printf("lista de pecados \n");
	fprintf(ar,"*pecados*\n");
	while(aux){
		printf("categoria %s : ",aux->nombre);
		fprintf(ar, "categoria- %s :",aux->nombre);
		pe* aux2=aux->proxp;
		printf("pecados: ");
		fprintf(ar,"pecados- ");
		while(aux2){
			printf("%s valor: %d, ",aux2->nombre,aux2->valor);
			fprintf(ar,"%s, ",aux2->nombre);
			aux2=aux2->prox; 
		} 
		printf("\n");
		fprintf(ar,".\n");
		aux=aux->proxc; 
	} 

	fclose(ar);
}
void mostrar_categoria_bondad (cabo*p){
	cabo *aux=p;
	printf("categorias de bondades.\n");
	printf("categorias: ");
	while(aux){
		printf("%s valor: %d\n",aux->nombre,aux->valor);
		aux=aux->proxc;
	}
}
void mostrar_bondades_categoria (cabo*p,char nom[]){
	cabo *aux=p;
	printf("bondades por categoria\n");
	printf("categoria %s valor: %d\n",aux->nombre,aux->valor);
	while(aux){
		if(!strcoll(aux->nombre,nom)){
			bo*auxp=aux->proxb;
			printf("bondades: ");
			while(auxp){
				printf("%s valor: %d, ",auxp->nombre, auxp->valor);
				auxp=auxp->prox;
			}
		}
		aux=aux->proxc;
	}
	printf("\n");
}
void mostrar_bondades_completa (cabo*p){
	cabo *aux=p;
	FILE *ar;
	char nombre[]="data2.txt";
	ar=fopen(nombre,"w");
	printf("lista de bondades \n");
	fprintf(ar,"*bondad*\n");
	while(aux){
		printf("categoria %s : ",aux->nombre);
		fprintf(ar, "categoria- %s :",aux->nombre);
		bo* aux2=aux->proxb;
		printf("bondades: ");
		fprintf(ar,"bondad- ");
		while(aux2){
			printf("%s valor: %d, ",aux2->nombre,aux2->valor);
			fprintf(ar,"%s, ",aux2->nombre);
			aux2=aux2->prox; 
		} 
		printf("\n");
		fprintf(ar,".\n");
		aux=aux->proxc; 
	} 

	fclose(ar);
}
void eliminar_pecado(char nom[],cape *p){
	cape *aux=p;
	pe *auxp,*ant;
	int bool=0;
	while(aux){
		auxp=aux->proxp;
		while(auxp){
			if(!strcoll(auxp->nombre,nom)){
				if (auxp==aux->proxp)
				{
					aux->proxp=auxp->prox;
					free(auxp);
				}else{
					ant->prox=auxp->prox;
					free(auxp);
				}
				printf("se elimino el pecado %s\n", nom);
				bool=1;
			}
			ant=auxp;
			auxp=auxp->prox;
		}
		aux=aux->proxc;
	}
	if (!bool)printf("no se encotro pecado\n");
}
void eliminar_bondad(char nom[],cabo *p){
	cabo *aux=p;
	bo *auxb,*ant;
	int bool=0;
	while(aux){
		auxb=aux->proxb;
		while(auxb){
			if(!strcoll(auxb->nombre,nom)){
				
				if (auxb==aux->proxb)
				{
					aux->proxb=auxb->prox;
					free(auxb);
				}else{
					ant->prox=auxb->prox;
					free(auxb);
				}
				printf("se elimino la bondad %s\n",nom);
				bool=1;
			}
			ant=auxb;
			auxb=auxb->prox;
		}
		aux=aux->proxc;
	}
	if (!bool)printf("no se encotro bondad\n");
}
void eliminar_categoria_pecado(char nom[],cape **p){
	cape * aux=*p,*ant;
	int bool=0;
	while(aux){
		if(!strcoll(aux->nombre,nom)){
			if(!aux->proxp){	
				if (*p==aux)
					{
						*p=aux->proxc;
						free(aux);
					}else{
						ant->proxc=aux->proxc;
						free(aux);
					}
				printf("se elimino la categoria %s\n",nom);
				bool=1;
			}else {bool=1;printf("la categoria no debe tener pecados eliminelos");}
		}
		ant=aux;
		aux=aux->proxc;
	}
if (!bool)printf("no se encotro categoria\n");
}
void eliminar_categoria_bondades(char nom[],cabo **p){
	cabo * aux=*p,*ant;
	int bool=0;
	while(aux){
		if(!strcoll(aux->nombre,nom)){
			if(!aux->proxb){	
				if (*p==aux)
					{
						*p=aux->proxc;
						free(aux);
					}else{
						ant->proxc=aux->proxc;
						free(aux);
					}
				printf("se elimino la categoria %s\n",nom);
				bool=1;
			}else{bool=1; printf("la categoria no debe tener bondades  eliminelas");}
		}
		ant=aux;
		aux=aux->proxc;
	}
if (!bool)printf("no se encotro categoria\n");
}
void modificar_valoracion_pecados (char nom[],int valor,cape* p){
	cape *aux=p;
	pe * auxp;
	int bool=1;
	while(aux){
		if (!strcoll(aux->nombre,nom))
		{	
			aux->valor=valor;
			printf("se cambio el valor de la categoria: %s\n",aux->nombre);
			bool =0;
		}
		else{
			auxp=aux->proxp;
			while(auxp){
				if (!strcoll(auxp->nombre,nom))
				{	
					auxp->valor=valor;
					printf("se cambio el valor de el pecado: %s\n",auxp->nombre);
					bool=0;
				}
				auxp=auxp->prox;
			}
		}
		aux=aux->proxc;
	}
	if (bool) printf("no se encontro pecado ni categoria");

}
void modificar_valoracion_bondades(char nom[],int valor,cabo* p){
	cabo *aux=p;
	bo * auxp;
	int bool=1;
	while(aux){
		if (!strcoll(aux->nombre,nom))
		{	
			aux->valor=valor;
			printf("se cambio el valor de la categoria: %s \n",aux->nombre);
			bool =0;
		}
		else{
			auxp=aux->proxb;
			while(auxp){
				if (!strcoll(auxp->nombre,nom))
				{	
					auxp->valor=valor;
					printf("se cambio el valor de la bondad: %s \n",auxp->nombre);
					bool=0;
				}
				auxp=auxp->prox;
			}
		}
		aux=aux->proxc;
	}
	if (bool) printf("no se encontro bondad ni categoria\n");

}
void mostrar_comportamiento(per *p){
	pb*bondad=p->bondad;
	pp*maldad=p->maldad;
	printf("%s\n",p->nombre );
	if(maldad){
		printf("maldades:");
		while(maldad){
			printf("%s fecha: %s", maldad->nombre,maldad->fecha);
			maldad=maldad->prox;
		}
	}else printf("no tiene pecados ");
	if(bondad){
		printf("\nbondades:");
		while(bondad){
			printf("%s fecha: %s", bondad->nombre,bondad->fecha);
			bondad=bondad->prox;
		}	
	}else printf("no tiene buenas acciones\n");

	printf("indice de maldad: %d\n",p->inmaldad );
	printf("indice de bondad: %d\n",p->inbondad );
}
int presentacion(){
	printf("bienvenido\n");
	printf("1.-para ver el proyecto\n 0.-para salir (opcion por defecto)\n");
	int opcion;
	opcion=0;// opcion por default
	fflush(stdin);
	scanf("%d",&opcion);
	if (opcion!=1){
		return 0;
	}
	return opcion;
}
void iniciar_listas(vi ** x,cabo ** y,cape **z){
	*x=NULL;
	*y=NULL;
	*z=NULL;
}
void datos_accion(char accion[15],char nombre[], int * valor,char cate[]){
	system("cls");
	char linea[45];
	*valor=100;// valor por default
	printf("ingrese el nombre de %s\n", accion);
	fflush(stdin);
	scanf("%[^\n]s",linea);
	strcpy(nombre,linea);
	fflush(stdin);
	printf("ingrese el valor de %s\n",accion );
	scanf("%d",valor);
	printf("ingrese el nombre de la categoria\n");
	fflush(stdin);
	scanf("%[^\n]s",linea);
	strcpy(cate,linea);

}
void datos_categoria(char nombre[],int *valor){
	system("cls");
	char linea[45];
	*valor=100; // valor por default
	printf("ingrese el nombre de la categoria\n");
	fflush(stdin);
	scanf("%[^\n]s",linea);
	strcpy(nombre,linea);
	fflush(stdin);
	printf("ingrese el valor de la categoria\n");
	scanf("%d",valor);

}
void datos_persona(char nombre[],char fecha[],char *genero,char pais[]){
	system("cls");
	int dia,mes,ano,numerog;
	char linea[45],numero[45];
	printf("ingrese el nombre de la pesona \n");
	fflush(stdin);
	scanf("%[^\n]s",linea);
	strcpy(nombre,linea);
	do
		{	fflush(stdin);
			printf("el dia de nacimiento \n");
			scanf("%d",&dia);
			if(dia<=0||dia>31){dia =0; printf("dia invalido\n" );}
			fflush(stdin);
			printf("el mes de nacimiento \n");
			scanf("%d",&mes);
			if(mes<=0||mes>12){mes =0;printf("mes invalido\n" );}
			fflush(stdin);
			printf("el ano de nacimiento \n");
			scanf("%d",&ano);
			if(ano<=0||ano>2015){ano =0;printf("ano invalido\n" );}
			if(mes==2&&dia >28){dia=0; printf("febrero solo tiene 28 dias\n");}

		} while((dia<=0)||(mes<=0)||(ano<=0));	
		itoa(dia,numero,10);
		strcpy(fecha,numero);
		strncat (fecha,"-",1);
		itoa(mes,numero,10);
		strncat (fecha,numero,2);
		strncat (fecha,"-",1);
		itoa(ano,numero,10);
		strncat(fecha,numero,4);
		printf("ingrese el el pais de la perosna \n");
		fflush(stdin);
		scanf("%[^\n]s",linea);
		strcpy(pais,linea);
		printf("ingrese el genero de la persona \n");
		printf("1.-masculino\n 2.-femenino\n");
		fflush(stdin);
		scanf("%d",&numerog);
		switch(numerog){
		  case 1: *genero='M';
			break;
		  case 2: *genero='F';
			break;
			default :
			printf("valor incorrecto");
			break;
		}

}
void datos_busqueda(char fecha[],int *con){
	system("cls");
	int dia,mes,ano;
	char linea[45],numero[45];
	*con=1;//valor por default
	do
		{	fflush(stdin);
			printf("el dia de nacimiento \n");
			scanf("%d",&dia);
			if(dia<=0||dia>31){dia =0; printf("dia invalido\n" );}
			fflush(stdin);
			printf("el mes de nacimiento \n");
			scanf("%d",&mes);
			if(mes<=0||mes>12){mes =0;printf("mes invalido\n" );}
			fflush(stdin);
			printf("el ano de nacimiento \n");
			scanf("%d",&ano);
			if(ano<=0||ano>2015){ano =0;printf("ano invalido\n" );}
			if(mes==2&&dia >28){dia=0; printf("febrero solo tiene 28 dias\n");}

		} while((dia<=0)||(mes<=0)||(ano<=0));	
		itoa(dia,numero,10);
		strcpy(fecha,numero);
		strncat (fecha,"-",1);
		itoa(mes,numero,10);
		strncat (fecha,numero,2);
		strncat (fecha,"-",1);
		itoa(ano,numero,10);
		strncat(fecha,numero,4);
		fflush(stdin);
		printf("el consecutivo por fecha de la persona \n");
		scanf("%d",con);

}
void datos_comportamiento(char accion[],char nombre[45],char fecha[45]){
	int dia,mes,ano;
	char linea[45],numero[45];
	do
		{	fflush(stdin);
			printf("el dia de la %s\n",accion);
			scanf("%d",&dia);
			if(dia<=0||dia>31){dia =0; printf("dia invalido\n" );}
			fflush(stdin);
			printf("el mes de la %s \n",accion);
			scanf("%d",&mes);
			if(mes<=0||mes>12){mes =0;printf("mes invalido\n" );}
			fflush(stdin);
			printf("el ano de de la %s \n",accion);
			scanf("%d",&ano);
			if(ano<=0||ano>2015){ano =0;printf("ano invalido\n" );}
			if(mes==2&&dia >28){dia=0; printf("febrero solo tiene 28 dias\n");}

		} while((dia<=0)||(mes<=0)||(ano<=0));	
		itoa(dia,numero,10);
		strcpy(fecha,numero);
		strncat (fecha,"-",1);
		itoa(mes,numero,10);
		strncat (fecha,numero,2);
		strncat (fecha,"-",1);
		itoa(ano,numero,10);
		strncat(fecha,numero,4);
		fflush(stdin);
		printf("el nombre de la %s \n",accion);
		scanf("%[^\n]s",linea);
		strcpy(nombre,linea);

}
void datos_busqueda_accion(char accion[],char nombre[],int *valor){
	system("cls");
	char linea[45];
	*valor=100; // valor por defalut
	fflush(stdin);
	printf("el nombre de la %s \n",accion);
	scanf("%[^\n]s",linea);
	fflush(stdin);
	strcpy(nombre,linea);
	printf("el valor de la %s \n",accion);
	scanf("%d",valor);
}
dato_eliminacion(char accion[],char nombre[]){
	system("cls");
	char linea[45];
	fflush(stdin);
	printf("ingrese el nombre de la %s \n",accion);
	scanf("%[^\n]s",linea);
	strcpy(nombre,linea);
}
void menu(){
	int opcion=presentacion();
	if(opcion){
		 char nombre[45],pais[45],genero,fecha[45],fechaA[45],categoria[45];
		 int valor,consecutivo,bool,fallo;
		vi *vivos;cabo *bondades;cape *maldad;
		 per * persona=NULL;
		iniciar_listas(&vivos,&bondades,&maldad);
		while(opcion){
			opcion=100;
		  system("cls");
		  fflush(stdin);
		  printf("1.-insertar categoria de bondad\n");
		  printf("2.- insertar bondad\n");
		  printf("3.-insertar categoria de pecado\n");
		  printf("4.- insertar pecado\n");
		  printf("5.- nacimiento de persona\n");
		  printf("6.- mostrar comportamiento de personas\n");
		  printf("7.- realizar buena accion\n");
		  printf("8.- cometer falta  \n");
		  printf("9.- mostrar listas bondades completa\n");
		  printf("10.- mostrar listas de pecados completa \n");
		  printf("11.- mostrar categorias de la lista de bondad\n");
		  printf("12.- mostrar categorias de la lista de pecados\n");
		  printf("13.- mostrar listas bondades por categoria\n");
		  printf("14.- mostrar listas de pecados por categoria \n");
		  printf("15.- mostrar todas las personas  \n");
		  printf("16.- modificar valoracion de bondades\n");
		  printf("17.- modificar valoracion de pecados\n");
	      printf("18.-eliminar categoria bondad\n");
	   	  printf("19.-eliminar bondad\n");
	   	  printf("20.-eliminar categoria pecado\n");
	   	  printf("21.-eliminar pecado\n");
		  printf("0.-salir\n");
		  scanf("%d",&opcion);
		  switch(opcion){
		  case	1:
		  		datos_categoria(nombre,&valor);
		   		insertar_categoria_bondad(nombre,valor,&bondades); 
		  		printf("categoria: %s se inserto correctamente\n",nombre ); 
		  		system("pause");
		  break;
		  case	2:
		  			datos_accion("la bondad",nombre,&valor,categoria);
		  			if(!buscar_bondad(bondades,nombre)){
		  				insertar_bondad(nombre,valor,categoria,bondades,&bool);
		  				if(bool)printf("la bondad: %s se inserto correctamente\n",nombre ); 
		  			}else printf("bondad  repetida\n");	
		  			system("pause");
		  	break;
		  case	3:
		  		datos_categoria(nombre,&valor);
		   		insertar_categoria_pecado(nombre,valor,&maldad); 
		  		printf("categoria: %s insertada correctamente\n",nombre );
		  		system("pause"); 
		  	break;
		  case	4: 
		  			datos_accion("el pecado",nombre,&valor,categoria);
		  			if(!buscar_pecado(maldad,nombre)){
		  				insertar_pecado(nombre,valor,categoria,maldad,&bool);
		  				if(bool)printf("el pecado: %s se inserto correctamente\n",nombre ); 
		  			}else printf("pecado repetido\n");
		  			 system("pause");
		  			
		  break;
		  case	5:  datos_persona(nombre,fecha,&genero,pais);
		  			consecutivo=0;
		  			nacimiento(&vivos,nombre,fecha,genero,pais,&consecutivo);
		  			if (consecutivo){
		  				printf("%s nacio correctamente con el consecutivo: %d en la fecha: %s\n",nombre,consecutivo,fecha);
		  			}
		  			system("pause");
		  break;
		  case	6:
		  			datos_busqueda(fecha,&consecutivo);
		  			persona = buscar_persona(vivos,fecha,consecutivo);
		  			if (persona)mostrar_comportamiento(persona);
		  			else printf("no se consiguio a la persona\n");
		  			system("pause");
		  break;
		  case	7: datos_busqueda(fecha,&consecutivo);
		  			datos_comportamiento("buena accion",nombre,fechaA);
		  			realizar_buena_accion(vivos,bondades,nombre,fechaA,fecha,consecutivo,&fallo);
		  			if(!fallo)printf("se realizo la buena accion\n" );
		  			system("pause");
		  break;
		  case	8: datos_busqueda(fecha,&consecutivo);
		  			datos_comportamiento("mala accion",nombre,fechaA);
		  			cometer_falta(vivos,maldad,nombre,fechaA,fecha,consecutivo,&fallo);
		  			if(!fallo)printf("se realizo la mala accion\n");
		  			system("pause");
		  break;
		  case	9: system("cls");
		  			mostrar_bondades_completa(bondades);
		  		system("pause");
		  break;
		  case	10: system("cls");
		  			mostrar_pecados_completa(maldad);
		  			system("pause");
		  break;
		  case	11:system("cls");
		  		 printf("nombre de la categoria de bondades\n");
				  	scanf("%s",nombre);
				  	mostrar_bondades_categoria(bondades,nombre);
				  system("pause");
		  break;
		  case	12: system("cls");
		  			printf("nombre de la categoria de pecados\n"); 
					  scanf("%s",nombre);
					  mostrar_pecados_categoria(maldad,nombre);
					  system("pause");
		  break;
		  case 13:	system("cls");
		  			mostrar_categoria_bondad(bondades); system("pause");
		  break;
		   case	14:	system("cls");
		   			mostrar_categoria_pecado(maldad); system("pause");
		  break;
		 case 15: system("cls");
		 			mostrar_persona(vivos);
					  system("pause");
		  break;
		  case 16: datos_busqueda_accion("buena accion",nombre,&valor); modificar_valoracion_bondades(nombre,valor,bondades); system("pause");
		  break;
		   case	17: datos_busqueda_accion("mala accion",nombre,&valor); modificar_valoracion_pecados(nombre,valor,maldad); system("pause");
		  break;
		   case	18: dato_eliminacion("categoria  bondad",nombre); eliminar_categoria_bondades(nombre,&bondades); system("pause");
		  break;
		   case	19:dato_eliminacion("bondad",nombre); eliminar_bondad(nombre,bondades); system("pause");
		  break;
		   case	20:dato_eliminacion("categoria  pecado",nombre); eliminar_categoria_pecado(nombre,&maldad); system("pause");
		  break;
		   case	21:dato_eliminacion("mala accion",nombre); eliminar_pecado(nombre,maldad); system("pause");
		  break;
		  case 0: break;
		  default:
		  printf("opcion invalida\n");
		  system("pause");
		  break;
		  }
		}
	}
	if(!opcion)printf("Hasta luego\n");
}
int main(int argc, char *argv[])
{
	
	menu();
	system("pause");
	return 0;
}
