#include <stdio.h>
#include <stdlib.h>
typedef struct pila{
	int valor;
	struct pila *sig;
}pil;
void crear_pila(pil ** p);
int esvaciap(pil*p);
int tope(pil * p);
void apilar(pil ** p,int v);
void desapilar(pil ** p);
void mostrar (pil * p);
void restar(pil * c,pil * b, pil ** a);
int repite(pil * b,int v);
void fraciones(pil *x,*y,pil *z);
int main(int argc, char const *argv[]){ 	
	pil * c,*b,*a;
	
	return 0;
}
void fraciones(pil *x,*y,pil **z){
	int j=0,i=0;
	i=tope(y);

}
void restar(pil * c,pil * b, pil ** a){
	int v;
	if (!esvaciap(c)){
		v=tope(c);
		desapilar(&c);
		restar(c,b,a);
		if(!repite(b,v))
			apilar(a,v);
		apilar(&c,v);
	}
	else 
	 crear_pila(a);
}
int repite(pil * b,int v){
	int x,num;
	if (!esvaciap(b)){
		x=tope(b);
		desapilar(&b);
		if(x!=v)
			num= repite(b,v);
		else
			num= repite(b,v)+1;
		apilar(&b,x);
		return num;
	}
	else
		return 0;
}
void apilar(pil ** p,int v){
	pil * nuevo;
	nuevo =(pil*) malloc(sizeof(pil));
	nuevo->valor=v;
	nuevo->sig=*p;
	*p=nuevo;
}
void desapilar(pil ** p){
	pil *aux=*p;
	*p=(*p)->sig;
	free(aux);

}
void crear_pila(pil ** p){
	*p=NULL;
}
int esvaciap(pil*p){
	if (!p)
		return 1;
	else
		return 0;
}
int tope(pil * p){	
	if (!esvaciap(p))
		return p->valor;
}
void mostrar (pil * p){
	if(!esvaciap(p)){
		int v=tope(p);
		printf("%d\n",v);
		desapilar(&p);
		mostrar(p);
		apilar(&p,v);
	}
}
