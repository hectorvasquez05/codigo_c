#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*--------------------------------------------------------------------- estructuras---------------------------------------------*/
typedef struct pecado{
	char nombre[45];
	int valor;
	struct pecado *prox; 
	struct pecado *ant; 
}pe;
typedef struct persona_pecado{
	char nombre[45],fecha[45];
	int valor;
	struct persona_pecado *prox; 
	struct persona_pecado *ant; 
}pp;


typedef struct categoria_pecados{
	char nombre[45];
	int valor;
	struct categoria_pecados *proxc;
	struct categoria_pecados *antc;
	pe *proxp; 
}cape;

typedef struct bondad{
	char nombre[45];
	int valor;
	struct bondad *prox; 
	struct bondad *ant; 
}bo;
typedef struct persona_bondad{
	char nombre[45], fecha[45];
	int valor;
	struct persona_bondad *prox;
	struct persona_bondad *ant;  
}pb;

typedef struct categoria_bondad{
	char nombre[45];
	int valor;
	struct categoria_bondad *proxc;
	struct categoria_bondad *antc;
	bo *proxb; 
}cabo;

typedef struct personas{
	char nombre[45], nacimiento[45], genero, pais[45];
	int inmaldad,inbondad,consecutivo,inexpiacion;
	pp *maldad;
	pb* bondad;
	struct personas* proxper;
	struct personas* antper;
}per;
typedef struct vivos{
	char fecha[45];
	struct vivos *proxv;
	struct vivos *antv;
	per * proxpe;
	
}vi;
/* ----------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------- buscar en listas----------------------------------------------------*/
vi * buscar_fecha(vi *vivos ,char fecha[]){
	vi* aux=vivos;
	while((aux)&&(strcoll(aux->fecha,fecha)))
			aux=aux->proxv;
	return aux;}
per * buscar_persona(vi * fecha,int consecutivo){
	per * aux=fecha->proxpe;
	while((aux)&&(consecutivo!=aux->consecutivo) )
		aux=aux->proxper;
	return aux;
}
cape * buscar_categoria_pecado(cape *categoria ,char nombre[]){
	cape* aux=categoria;
	while((aux)&&(strcoll(aux->nombre,nombre)))
		aux=aux->proxc;
	return aux;
}
cabo * buscar_categoria_bondad(cabo *categoria ,char nombre[]){
	cabo* aux=categoria;
	while((aux)&&(strcoll(aux->nombre,nombre)))
		aux=aux->proxc;
	return aux;
}
pe * buscar_pecado(cape *categoria ,char nombre[]){
	pe* aux=categoria->proxp;
	while((aux)&&(strcoll(aux->nombre,nombre)))
		aux=aux->prox;
	return aux;
}
bo * buscar_bondad(cabo *categoria ,char nombre[]){
	bo* aux=categoria->proxb;
	while((aux)&&(strcoll(aux->nombre,nombre)))
		aux=aux->prox;
	return aux;
}
pe*buscar_pecado_encategoria(cape *primer, char nombre[]){
    cape *aux=primer;
    pe * auxp;
    while(aux){
    	auxp=buscar_pecado(aux,nombre);
    	if (auxp) return auxp;
    	aux=aux->proxc;	
    }
    return auxp;
}
bo*buscar_bondad_encategoria(cabo *primer, char nombre[]){
    cabo *aux=primer;
    bo * auxp;
    while(aux){
    	auxp=buscar_bondad(aux,nombre);
    	if (auxp) return auxp;
    	aux=aux->proxc;
    	
    }
    return auxp;
}


/* ----------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------- iniciar listas----------------------------------------------------------*/
	void insertar_categoria_pecado (char ca[], int x, cape **p)
{
	cape *nuevo;
	nuevo = (cape*)malloc(sizeof(cape));
	nuevo->valor = x;
	strcpy(nuevo->nombre,ca);
	nuevo->proxc = *p;
	nuevo->proxp = NULL ;
	nuevo->antc = NULL;
	if(*p) (*p)->antc=nuevo;
	*p = nuevo;
}

void insertar_categoria_bondad (char ca[], int x, cabo **p)
{
	cabo *nuevo;
	nuevo = (cabo*)malloc(sizeof(cabo));
	nuevo->valor = x;
	strcpy(nuevo->nombre,ca);
	nuevo->proxc =*p;
	nuevo->proxb = NULL;
	nuevo->antc = NULL;
	if(*p) (*p)->antc=nuevo;
	*p = nuevo;
}
void insertar_pecado(cape*categoria,int x,char nombre[]){
	pe *aux=categoria->proxp;
	pe* nuevo=(pe*)malloc(sizeof(pe));
	nuevo->valor=x;
	strcpy(nuevo->nombre,nombre);
	nuevo->prox =aux;
	nuevo->ant=NULL;
	if (aux) aux->ant =nuevo;
	categoria->proxp=nuevo;

}
void insertar_bondad(cabo* categoria,int x,char nombre[]){
	bo *aux=categoria->proxb;
	bo* nuevo=(bo*)malloc(sizeof(bo));
	nuevo->valor=x;
	strcpy(nuevo->nombre,nombre);
	nuevo->prox =aux;
	nuevo->ant=NULL;
	if (aux) aux->ant =nuevo;
	categoria->proxb=nuevo;

}
/*void nacimiento (vi ** p,char nom[],char fecha[],char g, char pa[],int *con){
	per * nuevop,*auxp;
	vi * aux=*p,*nuevov;
	int encontro=0;
	while((aux) && (!encontro)){
		if (!strcoll(aux->fecha,fecha)){
			nuevop=(per*)malloc(sizeof(per));
			strcpy(nuevop->nombre,nom);
			strcpy(nuevop->nacimiento,fecha);
			nuevop->inbondad=0;
			nuevop->inmaldad=0;
			nuevop->inexpiacion=0;
			nuevop->maldad=NULL;
			nuevop->bondad=NULL;
			nuevop->genero=g;
			strcpy(nuevop->pais,pa);
			auxp=aux->proxpe;
			while(auxp->proxper!=NULL){
				auxp=auxp->proxper;
			}
			nuevop->consecutivo=auxp->consecutivo+1;
			*con=nuevop->consecutivo;
			auxp->proxper=nuevop;
			nuevop->proxper=NULL;
			encontro=1;
		}
		aux=aux->proxv;
	}
	if (!encontro){
		nuevov =(vi*)malloc(sizeof(vi));
		strcpy(nuevov->fecha,fecha);
		nuevov->proxv=*p;
		*p=nuevov;
		nuevop=(per*)malloc(sizeof(per));
		strcpy(nuevop->nombre,nom);
		strcpy(nuevop->nacimiento,fecha);
		nuevop->consecutivo=1;
		*con=1;
		nuevop->genero=g;
		strcpy(nuevop->pais,pa);
		nuevov->proxpe=nuevop;
		nuevop->inbondad=0;
		nuevop->inmaldad=0;
		nuevop->inexpiacion=0;
		nuevop->maldad=NULL;
		nuevop->bondad=NULL;
		nuevop->proxper=NULL;
	}

}*/
/* ----------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------eliminar listas-----------------------------------------------------*/
void eliminar_pecado(pe* pecado,cape *categoria){
	if (pecado->ant)
		pecado->ant=pecado->prox;
	else  categoria->proxp=pecado->prox;
	free(pecado);
}
void eliminar_bondad(bo* bondad,cabo *categoria){
	if (bondad->ant)
		bondad->ant=bondad->prox;
	else  categoria->proxb=bondad->prox;
	free(bondad);
}
void eliminar_categoria_pecado(cape *categoria,cape **primera){
	if(!categoria->proxp){
		if (categoria->antc)
			categoria->antc=categoria->proxc;
		else *primera=categoria->proxc;
		free(categoria);
	}else printf("la categoria posee pecados asociados eliminelos primero\n");
}

void eliminar_categoria_bondades(cabo *categoria,cabo **primera){
	if(!categoria->proxb){
		if (categoria->antc)
			categoria->antc=categoria->proxc;
		else *primera=categoria->proxc;
		free(categoria);
	}else printf("la categoria posee bondades asociadas eliminelas primero\n");
}
/* ----------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------- mostrar en listas----------------------------------------------------*/
void mostrar_pecado(pe* pecado){
	if(pecado)printf("pecado: %s valoracion: %d\n",pecado->nombre,pecado->valor );
	else printf("el pecado no existe \n");
}
void mostrar_categoria_pecado(cape* categoria){
	if(categoria)printf("categoria: %s valoracion: %d\n",categoria->nombre,categoria->valor );
	else printf("la categoria no existe \n");
}
void mostrar_pecados_categoria(cape* categoria){

}

void mostrar_categorias_pecado(cape* primer){}

void mostrar_pecado_completa(cape* primer){
	cape *aux=primer;
	pe* auxp;
	printf("lista de pecados\n");
	while(aux){
		mostrar_categoria_pecado(aux);
		auxp=aux->proxp;
		while(auxp){
		   mostrar_pecado(auxp);
		   auxp=auxp->prox;
		}
		aux=aux->proxc;
		printf("\n");
	}
}
void mostrar_bondad(bo* bondad){
	if(bondad)printf("bondad: %s valoracion: %d\n",bondad->nombre,bondad->valor );
	else printf("la bondad no existe \n");
}
void mostrar_categoria_bondad(cabo* categoria){
	if(categoria)printf("categoria: %s valoracion: %d\n",categoria->nombre,categoria->valor );
	else printf("la categoria no existe \n");
}
void mostrar_bondades_categoria(cabo* categoria){

}

void mostrar_categorias_bondad(cabo* primer){}

void mostrar_bondad_completa(cabo* primer){
	cabo *aux=primer;
	bo* auxp;
	printf("lista de bondades\n");
	while(aux){
		mostrar_categoria_bondad(aux);
		auxp=aux->proxb;
		while(auxp){
		   mostrar_bondad(auxp);
		   auxp=auxp->prox;
		}
		aux=aux->proxc;
		printf("\n");
	}
}

/* ----------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------- modificar en listas----------------------------------------------------*/
void modificar_pecado(char nombre[],cape * primer,int v){

	pe * aux = buscar_pecado_encategoria(primer,nombre);	
	aux->valor=v;	
}
void modificar_bondad(char nombre[],cabo * primer,int v){

	bo * aux = buscar_bondad_encategoria(primer,nombre);	
	aux->valor=v;	
}
void modificar_categoria_pecado(char nombre[],cape * primer,int v){

	cape * aux = buscar_categoria_pecado(primer,nombre);	
	aux->valor=v;	
}
void modificar_categoria_bondad(char nombre[],cabo * primer,int v){

	cabo * aux = buscar_categoria_bondad(primer,nombre);	
	aux->valor=v;	
}


/* ----------------------------------------------------------------------------------------------------------------------------------*/
int main(){
	cape *pecados=NULL;
	insertar_categoria_pecado("leve",50,&pecados);
	insertar_categoria_pecado("medio",100,&pecados);
	insertar_pecado(buscar_categoria_pecado(pecados,"leve"),5,"mentir");
	insertar_pecado(buscar_categoria_pecado(pecados,"leve"),5,"insultar");
	insertar_pecado(buscar_categoria_pecado(pecados,"medio"),25,"hurto");
	mostrar_pecado_completa(pecados);

	modificar_categoria_pecado("leve",pecados,100);
	modificar_pecado("mentir",pecados,10);
	mostrar_pecado_completa(pecados);
	mostrar_pecado(buscar_pecado_encategoria(pecados,"mentir"));
	eliminar_categoria_pecado(buscar_categoria_pecado(pecados,"medio"),&pecados);
	mostrar_pecado_completa(pecados);
	eliminar_pecado(buscar_pecado_encategoria(pecados,"hurto"),pecados);
	mostrar_pecado_completa(pecados);
	eliminar_categoria_pecado(buscar_categoria_pecado(pecados,"medio"),&pecados);
	mostrar_pecado_completa(pecados);

	system("pause");
	return 0;
}
